

class Cross(object):
	"""docstring for Cross"""
	def __init__(self):
		super(Cross, self).__init__()
		
		self._symbol = "X"

	def __str__(self):
		return self._symbol

	def get_symbol(self):
		return self._symbol


