

class Circle(object):
	"""docstring for Circle"""
	def __init__(self):
		super(Circle, self).__init__()

		self._symbol = "O"

	def __str__(self):
		return self._symbol

	def get_symbol(self):
		return self._symbol
				

	