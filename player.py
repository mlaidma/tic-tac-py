
class Player(object):
	"""docstring for Player"""
	def __init__(self, name, symbol):
		super(Player, self).__init__()
		
		self._name = name
		self._symbol = symbol

	def move(self):
		move = input(self._name + " käib: ")
		print(self._name + " käis " + move)
		return move

	def get_symbol(self):
		return str(self._symbol)
