
from player import Player
from circle import Circle
from cross import Cross
from board import Board

board = Board()
board.build()

#Create the players
player1_name = input("Sisesta 1. mängija nimi: ")
player2_name = input("Sisesta 2. mängija nimi: ")

player1 = Player(player1_name, Circle())
player2 = Player(player2_name, Cross())

players = [player1, player2]

print("Mängu alustavad " + player1._name + " ja " + player2._name)
print(player1._name + " mängib sümboliga: " + str(player1._symbol))
print(player2._name + " mängib sümboliga: " + str(player2._symbol))

game_finished = False
nr_of_moves = 0

while not game_finished:
	for player in players:
		move = player.move()
		nr_of_moves += 1
		print("")
		winner = board.update(move, player.get_symbol())
		if winner != None:
			for p in players:
				if p.get_symbol() == winner:
					print(p._name + " VÕITIS!")
					game_finished = True
					break
		else:
			if nr_of_moves == 9:
				print("MÄNG JÄI VIIKI!")
				game_finished = True
				break
			else:
				continue
		break