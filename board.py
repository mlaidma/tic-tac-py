
from circle import Circle
from cross import Cross

class Board(object):
	"""docstring for Board"""
	def __init__(self):
		super(Board, self).__init__()

		self._width = 11
		self._height = 3

		self._row1 = 0
		self._row2 = 1
		self._row3 = 2

		self._colA = 1
		self._colB = 5
		self._colC = 9

		self._field = []

	def build(self):
		for row in range(self._height):
			row_content = []
			for col in range(self._width):
				if col == 3 or col == 7:
					row_content.append("|")
				else:
					if row != 2:
						row_content.append("_")
					else:
						row_content.append(" ")
			self._field.append(row_content)

	def draw(self):
		for row in range(self._height):
			for col in range(self._width):
				print(self._field[row][col], end = "")
			print("")
		print("")

	def get_cell(self, row, col):
		return self._field[row][col]

	def check(self):
		#Read
		if (self.get_cell(self._row1, self._colA) == "X" and
		   self.get_cell(self._row1, self._colB) == "X" and
		   self.get_cell(self._row1, self._colC) == "X"):
		   return "X"
		elif (self.get_cell(self._row1, self._colA) == "O" and
		   self.get_cell(self._row1, self._colB) == "O" and
		   self.get_cell(self._row1, self._colC) == "O"):
		   return "O"
		elif (self.get_cell(self._row2, self._colA) == "X" and
		   self.get_cell(self._row2, self._colB) == "X" and
		   self.get_cell(self._row2, self._colC) == "X"):
		   return "X"
		elif (self.get_cell(self._row2, self._colA) == "O" and
		   self.get_cell(self._row2, self._colB) == "O" and
		   self.get_cell(self._row2, self._colC) == "O"):
		   return "O"
		#Rida 3
		elif (self.get_cell(self._row3, self._colA) == "X" and
		   self.get_cell(self._row3, self._colB) == "X" and
		   self.get_cell(self._row3, self._colC) == "X"):
		   return "X"
		elif (self.get_cell(self._row3, self._colA) == "O" and
		   self.get_cell(self._row3, self._colB) == "O" and
		   self.get_cell(self._row3, self._colC) == "O"):
		   return "O"
		#Tulp A
		elif (self.get_cell(self._row1, self._colA) == "X" and
		   self.get_cell(self._row2, self._colA) == "X" and
		   self.get_cell(self._row3, self._colA) == "X"):
			return "X"
		elif (self.get_cell(self._row1, self._colA) == "O" and
		   self.get_cell(self._row2, self._colA) == "O" and
		   self.get_cell(self._row3, self._colA) == "O"):
		   return "O"
		#Tulp B
		elif (self.get_cell(self._row1, self._colB) == "X" and
		   self.get_cell(self._row2, self._colB) == "X" and
		   self.get_cell(self._row3, self._colB) == "X"):
		   return "X"
		elif (self.get_cell(self._row1, self._colB) == "O" and
		   self.get_cell(self._row2, self._colB) == "O" and
		   self.get_cell(self._row3, self._colB) == "O"):
		   return "O"
		#Tulp C
		elif (self.get_cell(self._row1, self._colC) == "X" and
		   self.get_cell(self._row2, self._colC) == "X" and
		   self.get_cell(self._row3, self._colC) == "X"):
		   return "X"
		elif (self.get_cell(self._row1, self._colC) == "O" and
		   self.get_cell(self._row2, self._colC) == "O" and
		   self.get_cell(self._row3, self._colC) == "O"):
		   return "O"
		#Diagonaal vasakult - paremale
		elif (self.get_cell(self._row1, self._colA) == "X" and
		   self.get_cell(self._row2, self._colB) == "X" and
		   self.get_cell(self._row3, self._colC) == "X"):
		   return "X"
		elif (self.get_cell(self._row1, self._colA) == "O" and
		   self.get_cell(self._row2, self._colB) == "O" and
		   self.get_cell(self._row3, self._colC) == "O"):
		   return "O"
		#Diagonaal paremalt vasakule
		elif (self.get_cell(self._row1, self._colC) == "X" and
		   self.get_cell(self._row2, self._colB) == "X" and
		   self.get_cell(self._row3, self._colA) == "X"):
		   return "X"
		elif (self.get_cell(self._row1, self._colC) == "O" and 
		   self.get_cell(self._row2, self._colB) == "O" and
		   self.get_cell(self._row3, self._colA) == "O"):
		   return "O"
		else:
			return None

	def update(self, move, symbol):

		move_ok = self.parse_move(move)

		if move_ok != None:
			self._field[move_ok[0]][move_ok[1]] = symbol
			self.draw()
			check = self.check()
			if check != None:
				return check
		else:
			return None

	def parse_move(self, move):
		row = None
		col = None
		if move[0] == "A":
			col = self._colA
		elif move[0] == "B":
			col = self._colB
		elif move[0] == "C":
			col = self._colC
		else:
			print("VIGA! Sellist tulpa pole")
			return None

		if move[1] == "1":
			row = self._row1
		elif move[1] == "2":
			row = self._row2
		elif move[1] == "3":
			row = self._row3
		else:
			print("VIGA! Sellist rida pole")
			return None

		return (row, col)
